package me.icyrelic.com.API;


import java.util.List;
import java.util.UUID;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;


public class LBApi {

	LegendaryBans plugin;
	public LBApi(LegendaryBans instance) {
		plugin = instance;
	}	
	private LBMySQL mysql = new LBMySQL();
	
	public boolean addWarning(UUID uid, String name, String admin, String reason){
		plugin.getServer().broadcastMessage(ChatColor.RED+name.toLowerCase()+ChatColor.GRAY+" has recieved a warning from "+ChatColor.RED+admin);
		plugin.getServer().broadcastMessage(ChatColor.GRAY + "     "+reason);
		return mysql.addWarning(uid, name, admin, reason);
	}
	public boolean checkBan(UUID uid){
		return mysql.checkBan(uid);
	}
	public int countWarningsByAdmin(UUID uid, String admin){
		return mysql.countWarningsByAdmin(uid, admin);
	}
	public int countWarnings(UUID uid){
		return mysql.countWarnings(uid);
	}
	public boolean clearWarnings(UUID uid){
		return mysql.clearWarnings(uid);
	}
	public boolean createBan(UUID uid, String name, String admin, int type, long unban, String reason){
		
		if(type == 0){
			//ban
			Bukkit.getServer().broadcastMessage(ChatColor.YELLOW+name.toLowerCase()+ChatColor.GOLD+" was banned by "+ChatColor.YELLOW+admin+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);
		}else if(type == 1){
			//tempban
			Bukkit.getServer().broadcastMessage(ChatColor.YELLOW+name.toLowerCase()+ChatColor.GOLD+" was tempbanned by "+ChatColor.YELLOW+admin+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);
		}else if(type == 2){
			//permban
			plugin.getServer().broadcastMessage(ChatColor.YELLOW+name.toLowerCase()+ChatColor.GOLD+" was permanently banned by "+ChatColor.YELLOW+admin+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);

		}
		
		@SuppressWarnings("deprecation")
		List<Player> possible = Bukkit.matchPlayer(name);
		
		Bukkit.getOnlinePlayers().toString().contains(name);
		if(possible.size() == 1){
			for(Player p : Bukkit.getOnlinePlayers()){
				if(p.getName().toLowerCase().equals(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase())){
					p.kickPlayer( ChatColor.GOLD +"You were banned by: "+ ChatColor.YELLOW +admin+ ChatColor.GOLD + " Reason: "+ ChatColor.YELLOW +reason);
				}
			}
		}
		
		return mysql.createBan(uid, name, admin, type, unban, reason);
	}
	
			

}
