package me.icyrelic.com.Listeners;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;


public class quit implements Listener {

	LegendaryBans plugin;
	public quit(LegendaryBans instance) {

		plugin = instance;

		}
	
	private LBMySQL mysql = new LBMySQL();
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerKick(PlayerKickEvent e){
			e.setLeaveMessage("");
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent e){
		if(mysql.checkBan(e.getPlayer().getUniqueId())){
			e.setQuitMessage("");
			
		}
	}
	
}
