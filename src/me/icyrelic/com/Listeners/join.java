package me.icyrelic.com.Listeners;

import java.text.SimpleDateFormat;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

public class join implements Listener {
	
	LegendaryBans plugin;
	public join(LegendaryBans instance) {

		plugin = instance;

		}
	
	private String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "LegendaryBans" + ChatColor.WHITE + "] ");
	
	private LBMySQL mysql = new LBMySQL();
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent e){
		
		if(mysql.checkBan(e.getPlayer().getUniqueId())){
			
			
			if(mysql.getBanType(e.getPlayer().getUniqueId()) == 0){
				//norman  ban
				String msg = (ChatColor.GOLD+"You are banned from this server!\n\nBanned By: "+ChatColor.YELLOW+mysql.getAdmin(e.getPlayer().getUniqueId())+ChatColor.GOLD+"\n\nBan Reason: "+ChatColor.YELLOW+mysql.getBanReason(e.getPlayer().getUniqueId()));
				e.disallow(PlayerLoginEvent.Result.KICK_OTHER, msg);
			}else if(mysql.getBanType(e.getPlayer().getUniqueId()) == 2){
				//permban
				String msg = (ChatColor.GOLD+"You are permanently banned from this server!\n\nBanned By: "+ChatColor.YELLOW+mysql.getAdmin(e.getPlayer().getUniqueId())+ChatColor.GOLD+"\n\nBan Reason: "+ChatColor.YELLOW+mysql.getBanReason(e.getPlayer().getUniqueId()));
				e.disallow(PlayerLoginEvent.Result.KICK_OTHER, msg);
			}else if(mysql.getBanType(e.getPlayer().getUniqueId()) == 1){
				//tempban
				
				long x = mysql.getUnban(e.getPlayer().getUniqueId());
				
				long y = x-System.currentTimeMillis();
				long z = y/1000;
				
				
                SimpleDateFormat ft = 
                new SimpleDateFormat ("yyyy-MM-dd h:mm aa");
                
                
                if(!(z <= 0)){
                	String msg = (ChatColor.GOLD+"You are banned until "+ChatColor.YELLOW+ft.format(x)+ChatColor.GOLD+"\n\n Banned By: "+ChatColor.YELLOW+mysql.getAdmin(e.getPlayer().getUniqueId())+ChatColor.GOLD+"\n\nBan Reason: "+ChatColor.YELLOW+mysql.getBanReason(e.getPlayer().getUniqueId()));
                	
                	e.disallow(PlayerLoginEvent.Result.KICK_OTHER, msg);
                }
				
				
			}else{
				//999 or unknown
			}
			
			
			
		}
		
		
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent e){
		
		if(mysql.checkBan(e.getPlayer().getUniqueId())){

			
			
			long x = mysql.getUnban(e.getPlayer().getUniqueId());
			
			long y = x-System.currentTimeMillis();
			long z = y/1000;
			
			long type = mysql.getBanType(e.getPlayer().getUniqueId());
			
            
            
            if(z <= 0 || type == 999){
				plugin.getServer().broadcastMessage(prefix + e.getPlayer().getName() + " has joined after being banned for: " + mysql.getBanReason(e.getPlayer().getUniqueId()));
				mysql.removeBan(e.getPlayer().getUniqueId());
            }
			
		}
		
		
		
		
	}
	
	
}
