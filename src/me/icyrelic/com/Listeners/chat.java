package me.icyrelic.com.Listeners;

import me.icyrelic.com.LegendaryBans;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class chat implements Listener {
	
	LegendaryBans plugin;
	public chat(LegendaryBans instance) {

		plugin = instance;

		}
	
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e){
		
		if(plugin.muted.containsKey(e.getPlayer().getName())){
			e.getPlayer().sendMessage(ChatColor.RED + "You have been muted Reason: "+plugin.muted.get(e.getPlayer().getName()));
			e.setCancelled(true);
			
		}
		
	}

}
