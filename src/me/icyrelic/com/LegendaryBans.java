package me.icyrelic.com;

import java.io.IOException;
import java.util.HashMap;

import me.icyrelic.com.API.LBApi;
import me.icyrelic.com.Commands.ban;
import me.icyrelic.com.Commands.checkban;
import me.icyrelic.com.Commands.clearwarnings;
import me.icyrelic.com.Commands.kick;
import me.icyrelic.com.Commands.mute;
import me.icyrelic.com.Commands.permban;
import me.icyrelic.com.Commands.tempban;
import me.icyrelic.com.Commands.unban;
import me.icyrelic.com.Commands.warn;
import me.icyrelic.com.Data.LBMySQL;
import me.icyrelic.com.Data.Updater;
import me.icyrelic.com.Data.Updater.UpdateResult;
import me.icyrelic.com.Data.Updater.UpdateType;
import me.icyrelic.com.Listeners.chat;
import me.icyrelic.com.Listeners.join;
import me.icyrelic.com.Listeners.quit;
import me.icyrelic.com.Metrics.LBMetrics;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class LegendaryBans extends JavaPlugin{

	private LBMySQL mysql = new LBMySQL();
	public final LBApi api = new LBApi(this);
	public String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "LegendaryBans" + ChatColor.WHITE + "] ");
	public HashMap<String, String> muted = new HashMap<String, String>(); 
	
	public void onEnable(){
		ConsoleCommandSender console = getServer().getConsoleSender();
		
		if(getConfig().getBoolean("AutoUpdate")){
			Updater check = new Updater(this, 62809, this.getFile(), UpdateType.NO_DOWNLOAD, true);
			
			if (check.getResult() == UpdateResult.UPDATE_AVAILABLE) {
			    console.sendMessage(prefix+"New Version Available! "+ check.getLatestName());
			    
				Updater download = new Updater(this, 39616, this.getFile(), UpdateType.DEFAULT, true);
				
			    if(download.getResult() == UpdateResult.SUCCESS){
			    	console.sendMessage(prefix+"Successfully Updated Please Restart To Finalize");
			    }
			    
			}else{
				console.sendMessage(prefix+"You are currently running the latest version of LegendaryBans");
			}
			


		}
		
		loadConfiguration();
		
		String host = getConfig().getString("Database.Hostname");
		String port = getConfig().getString("Database.Port");
		String db = getConfig().getString("Database.Database");
		String user = getConfig().getString("Database.Username");
		String password = getConfig().getString("Database.Password");
		
		if(mysql.connect(host, port, db, user, password)){
			System.out.println(prefix+"Connected To Database '"+db+"'");
			
			getCommand("ban").setExecutor(new ban(this));
			getCommand("permban").setExecutor(new permban(this));
			getCommand("unban").setExecutor(new unban(this));
			getCommand("tempban").setExecutor(new tempban(this));
			getCommand("checkban").setExecutor(new checkban(this));
			getCommand("kick").setExecutor(new kick(this));
			getCommand("mute").setExecutor(new mute(this));
			getCommand("warn").setExecutor(new warn(this));
			getCommand("clearwarnings").setExecutor(new clearwarnings(this));
			
			startMetrics();
			
			
			getServer().getPluginManager().registerEvents(new chat(this), this);
			getServer().getPluginManager().registerEvents(new join(this), this);
			getServer().getPluginManager().registerEvents(new quit(this), this);
			
			
			 
			
		}else{
			System.out.println(prefix+"Failed To Connect To Database '"+db+"'");
			System.out.println(prefix+"Disabling LegendaryBans");
			getServer().getPluginManager().disablePlugin(this);
			
		}
		

		
	}
	
	private void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	
	}
	
	private void startMetrics(){
		System.out.println("[LegendaryBans] Trying to start Metrics");
		 try {
			    LBMetrics metrics = new LBMetrics(this);
			    metrics.start();
			} catch (IOException e) {
				System.out.println(prefix+"Failed to send metrics");
		 }
	}

	

}
