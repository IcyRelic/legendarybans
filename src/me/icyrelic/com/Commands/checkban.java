package me.icyrelic.com.Commands;


import java.text.SimpleDateFormat;
import java.util.UUID;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.PUUID;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class checkban implements CommandExecutor {
	
	LegendaryBans plugin;
	public checkban(LegendaryBans instance) {

		plugin = instance;

		}
	
	private LBMySQL mysql = new LBMySQL();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("checkban")) {
			
			if(sender.hasPermission("LegendaryBans.checkban")){
				//checkban player reason
				if(args.length == 1){
					
					SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd h:mm aa");
					
					String player = args[0];
					
					if(!PUUID.UUIDExists(player)){
						sender.sendMessage(ChatColor.RED + "Error: Could not find player UID please check the username and try again!");
						return true;
					}
					
					UUID uid = null;
					try {
						uid = UUID.fromString(PUUID.getUUID(player));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(!mysql.checkBan(uid)){
						sender.sendMessage(ChatColor.GOLD + "============["+ChatColor.YELLOW + "Ban Information"+ChatColor.GOLD + "]============");
						sender.sendMessage(ChatColor.GOLD+"Name: " + ChatColor.YELLOW+player);
						sender.sendMessage(ChatColor.GREEN+"This player is not banned!");
						sender.sendMessage(ChatColor.GOLD + "====================================");

					}else{

						if(mysql.getBanType(uid) == 999){

							sender.sendMessage(ChatColor.GOLD + "============["+ChatColor.YELLOW + "Ban Information"+ChatColor.GOLD + "]============");
							sender.sendMessage(ChatColor.GOLD+"Name: " + ChatColor.YELLOW+player);
							sender.sendMessage(ChatColor.GREEN+"This player is not banned!");
							sender.sendMessage(ChatColor.GOLD + "====================================");
							
						}else if(mysql.getBanType(uid) == 0){

							long x = mysql.getDate(uid);
							
							sender.sendMessage(ChatColor.GOLD + "============["+ChatColor.YELLOW + "Ban Information"+ChatColor.GOLD + "]============");
							sender.sendMessage(ChatColor.GOLD+"Name: " + ChatColor.YELLOW+player);
							sender.sendMessage(ChatColor.GOLD+"Date: " + ChatColor.YELLOW+ft.format(x));
							sender.sendMessage(ChatColor.GOLD+"Ban Type: "+ChatColor.YELLOW+"ban");
							sender.sendMessage(ChatColor.GOLD+"Banned By: " +ChatColor.YELLOW+ mysql.getAdmin(uid));
							sender.sendMessage(ChatColor.GOLD+"Reason: " +ChatColor.YELLOW+ mysql.getBanReason(uid));
							sender.sendMessage(ChatColor.GOLD + "====================================");
							
						}else if(mysql.getBanType(uid) == 1){
							

							long x = mysql.getDate(uid);
							long y = mysql.getUnban(uid);
							
							sender.sendMessage(ChatColor.GOLD + "============["+ChatColor.YELLOW + "Ban Information"+ChatColor.GOLD + "]============");
							sender.sendMessage(ChatColor.GOLD+"Name: " + ChatColor.YELLOW+player);
							sender.sendMessage(ChatColor.GOLD+"Date: " +ChatColor.YELLOW+ ft.format(x));
							sender.sendMessage(ChatColor.GOLD+"Banned Until: "+ChatColor.YELLOW+ft.format(y));
							sender.sendMessage(ChatColor.GOLD+"Ban Type: "+ChatColor.YELLOW+"tempban");
							sender.sendMessage(ChatColor.GOLD+"Banned By: " +ChatColor.YELLOW+ mysql.getAdmin(uid));
							sender.sendMessage(ChatColor.GOLD+"Reason: " +ChatColor.YELLOW+ mysql.getBanReason(uid));
							sender.sendMessage(ChatColor.GOLD + "====================================");

						}else if(mysql.getBanType(uid) == 2){
							

							long x = mysql.getDate(uid);
							
							sender.sendMessage(ChatColor.GOLD + "============["+ChatColor.YELLOW + "Ban Information"+ChatColor.GOLD + "]============");
							sender.sendMessage(ChatColor.GOLD+"Name: " + ChatColor.YELLOW+player);
							sender.sendMessage(ChatColor.GOLD+"Date: " +ChatColor.YELLOW+ ft.format(x));
							sender.sendMessage(ChatColor.GOLD+"Ban Type: "+ChatColor.YELLOW+"permban");
							sender.sendMessage(ChatColor.GOLD+"Banned By: " +ChatColor.YELLOW+ mysql.getAdmin(uid));
							sender.sendMessage(ChatColor.GOLD+"Reason: " +ChatColor.YELLOW+ mysql.getBanReason(uid));
							sender.sendMessage(ChatColor.GOLD + "====================================");

						}
						
						
					}

					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /checkban <player>");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
}
