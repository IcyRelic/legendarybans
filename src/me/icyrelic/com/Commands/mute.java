package me.icyrelic.com.Commands;


import java.util.List;

import me.icyrelic.com.LegendaryBans;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class mute implements CommandExecutor {
	
	LegendaryBans plugin;
	public mute(LegendaryBans instance) {

		plugin = instance;

		}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("mute")) {
			
			if(sender.hasPermission("LegendaryBans.mute")){
				//mute player reason
				if(args.length >= 1){
					String reason;
					if(!(args.length >= 2)){
						reason = "Reason not specified";
					}else{
						reason = getFinalArg(args,1);
					}
					
					
					List<Player> possible = Bukkit.matchPlayer(args[0]);
					
					if(possible.size() == 1){
						Player p = plugin.getServer().getPlayer(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase());
					
						if(plugin.muted.containsKey(p.getName())){
							plugin.muted.remove(p.getName());
							plugin.getServer().broadcastMessage(ChatColor.YELLOW+possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase()+ChatColor.GOLD+" was unmuted by "+ChatColor.YELLOW+sender.getName());

						}else{
							plugin.muted.put(p.getName(), reason);
							plugin.getServer().broadcastMessage(ChatColor.YELLOW+possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase()+ChatColor.GOLD+" has been muted by "+ChatColor.YELLOW+sender.getName()+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);

						}
						
											
					}else{
						sender.sendMessage(ChatColor.RED + "Error: Player not found!");
					}

					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /mute <player> [<reason>]");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
	
}
