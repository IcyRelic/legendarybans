package me.icyrelic.com.Commands;

import java.util.List;
import java.util.UUID;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.PUUID;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class tempban implements CommandExecutor {
	
	LegendaryBans plugin;
	public tempban(LegendaryBans instance) {

		plugin = instance;

		}
	
	private LBMySQL mysql = new LBMySQL();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("tempban")) {
			
			if(sender.hasPermission("LegendaryBans.tempban")){
				//tempban player time format reason
				if(args.length >= 4){
					
					String player = args[0];
					String time = args[1];
					String format = args[2];
					String reason = getFinalArg(args, 3);
					
					String name = "";

					
					
					@SuppressWarnings("deprecation")
					List<Player> possible = Bukkit.matchPlayer(args[0]);
					if(possible.size() == 1){
						
						name = possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase();
						
					}else{
						
						name = player;
						
					}
					
					if(!PUUID.UUIDExists(name)){
						sender.sendMessage(ChatColor.RED + "Error: Could not find player UID please check the username and try again!");
						return true;
					}
					
					UUID uid = null;
					try {
						uid = UUID.fromString(PUUID.getUUID(name));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(mysql.checkBan(uid)){
						if(mysql.getBanType(uid) == 999){
							mysql.removeBan(uid);
						}
					}
					
					
					long x = System.currentTimeMillis();
					long z = 0;
					
					if(!mysql.checkBan(uid)){
						
						if(format.equals("year") || format.equals("month") || format.equals("week") || format.equals("day") || format.equals("hour") || format.equals("minute") || format.equals("second")){
							
							if(format.equals("year")){

								long t = Long.parseLong(time);
								
								long sec = 31536000*t;
								
								long milsec = sec*1000;
								
								z = milsec+x;
								
								
							}else if(format.equals("month")){
								
								long t = Long.parseLong(time);
								
								long sec = 2678400*t;
								
								long milsec = sec*1000;
								
								z = milsec+x;
								
								
							}else if(format.equals("week")){
								
								long t = Long.parseLong(time);
								
								long sec = 604800*t;
								
								long milsec = sec*1000;
								
								z = milsec+x;
								
							}else if(format.equals("day")){
								
								long t = Long.parseLong(time);
								
								long sec = 86400*t;
								
								long milsec = sec*1000;
								
								z = milsec+x;
								
							}else if(format.equals("hour")){
								
								long t = Long.parseLong(time);
								
								long sec = 3600*t;
								
								long milsec = sec*1000;
								
								z = milsec+x;
								
							}else if(format.equals("minute")){
								
								long t = Long.parseLong(time);
								
								long sec = 60*t;
								
								long milsec = sec*1000;
								
								z = milsec+x;
								
							}else if(format.equals("second")){
								
								long t = Long.parseLong(time);
								
								long sec = 1*t;
								
								long milsec = sec*1000;
								
								z = milsec+x;
								
							}
							
							
							mysql.createBan(uid, name, sender.getName(), 1, z, reason);
							
							
							
							
							if(possible.size() == 1){
								for(Player p : Bukkit.getOnlinePlayers()){
									if(p.getName().toLowerCase().equals(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase())){
										p.kickPlayer( ChatColor.GOLD +"You were tempbanned by: "+ ChatColor.YELLOW +sender.getName() + ChatColor.GOLD + " Reason: "+ ChatColor.YELLOW +reason);
										plugin.getServer().broadcastMessage(ChatColor.YELLOW+possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase()+ChatColor.GOLD+" was tempbanned by "+ChatColor.YELLOW+sender.getName()+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);
										plugin.getServer().broadcastMessage(ChatColor.YELLOW + name + ChatColor.GOLD + " is temporarily banned for " +ChatColor.YELLOW+  time +" " + format);
									}
								}
							}else if (possible.isEmpty()){
								plugin.getServer().broadcastMessage(ChatColor.YELLOW+name.toLowerCase()+ChatColor.GOLD+" was temporarily banned by "+ChatColor.YELLOW+sender.getName()+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);
								plugin.getServer().broadcastMessage(ChatColor.YELLOW + name + ChatColor.GOLD + " is temporarily banned for " +ChatColor.YELLOW+  time +" " + format);
								for(Player p : Bukkit.getOnlinePlayers()){
									if(p.getName().toLowerCase().equals(name)){
										p.kickPlayer( ChatColor.GOLD +"You were tempbanned by: "+ ChatColor.YELLOW +sender.getName() + ChatColor.GOLD + " Reason: "+ ChatColor.YELLOW +reason);
										
									}
								}
							}else{
								sender.sendMessage(ChatColor.RED + "Error: More than one player found");
							}
							
							
							
						}else{
							sender.sendMessage(ChatColor.RED + "Error: Invalid time format");
						}
						

						
					}else{
						sender.sendMessage(ChatColor.RED + "Error: Player is already banned!");
					}

					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /tempban <player> <time> <year|month|week|day|hour|minute|second> <reason>");
					
					
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
	
	
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}

}
