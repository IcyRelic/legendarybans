package me.icyrelic.com.Commands;

import java.util.List;
import java.util.UUID;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.PUUID;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class permban implements CommandExecutor {
	
	LegendaryBans plugin;
	public permban(LegendaryBans instance) {

		plugin = instance;

		}
	
	private LBMySQL mysql = new LBMySQL();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("permban")) {
			
			if(sender.hasPermission("LegendaryBans.permban")){
				//permban player reason
				if(args.length >= 2){
					
					String player = args[0];
					String reason = getFinalArg(args, 1);
					
					String name = "";
	
					
					@SuppressWarnings("deprecation")
					List<Player> possible = Bukkit.matchPlayer(args[0]);
					if(possible.size() == 1){
						
						name = possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase();
						
					}else{
						
						name = player;
						
					}
					
					if(!PUUID.UUIDExists(name)){
						sender.sendMessage(ChatColor.RED + "Error: Could not find player UID please check the username and try again!");
						return true;
					}
					
					UUID uid = null;
					try {
						uid = UUID.fromString(PUUID.getUUID(name));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(mysql.checkBan(uid)){
						if(mysql.getBanType(uid) == 999){
							mysql.removeBan(uid);
						}
					}
					
					
					if(!mysql.checkBan(uid)){
						mysql.createBan(uid, name, sender.getName(), 2, 0, reason);
						
						
						
						
						if(possible.size() == 1){
							for(Player p : Bukkit.getOnlinePlayers()){
								if(p.getName().toLowerCase().equals(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase())){
									p.kickPlayer( ChatColor.GOLD +"You were permbanned by: "+ ChatColor.YELLOW +sender.getName() + ChatColor.GOLD + " Reason: "+ ChatColor.YELLOW +reason);
									plugin.getServer().broadcastMessage(ChatColor.YELLOW+possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase()+ChatColor.GOLD+" was permanently banned by "+ChatColor.YELLOW+sender.getName()+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);
								}
							}
						}else if (possible.isEmpty()){
							plugin.getServer().broadcastMessage(ChatColor.YELLOW+name.toLowerCase()+ChatColor.GOLD+" was permanently banned by "+ChatColor.YELLOW+sender.getName()+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);
							for(Player p : Bukkit.getOnlinePlayers()){
								if(p.getName().toLowerCase().equals(name)){
									p.kickPlayer( ChatColor.GOLD +"You were permbanned by: "+ ChatColor.YELLOW +sender.getName() + ChatColor.GOLD + " Reason: "+ ChatColor.YELLOW +reason);

								}
							}
						}else{
							sender.sendMessage(ChatColor.RED + "Error: More than one player found");
						}
						
					}else{
						sender.sendMessage(ChatColor.RED + "Error: Player is already banned!");
					}

					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /permban <player> <reason>");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
	
	
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}

}
