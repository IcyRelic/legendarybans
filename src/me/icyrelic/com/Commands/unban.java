package me.icyrelic.com.Commands;


import java.util.UUID;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.PUUID;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class unban implements CommandExecutor {
	
	LegendaryBans plugin;
	public unban(LegendaryBans instance) {

		plugin = instance;

		}
	
	private LBMySQL mysql = new LBMySQL();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("unban")) {
			
			if(sender.hasPermission("LegendaryBans.unban")){
				//unban player reason
				if(args.length == 1){
					
					String player = args[0];
					
					if(!PUUID.UUIDExists(player)){
						sender.sendMessage(ChatColor.RED + "Error: Could not find player UID please check the username and try again!");
						return true;
					}
					
					
					UUID uid = null;
					try {
						uid = UUID.fromString(PUUID.getUUID(player));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(mysql.checkBan(uid)){
						
						if((mysql.getBanType(uid) == 999)){
							
							sender.sendMessage(ChatColor.RED + "Error: This player is not banned!");
							
						}else if(!(mysql.getBanType(uid) == 2)){
							
							mysql.setBanType("999",uid);
							
							plugin.getServer().broadcastMessage(ChatColor.YELLOW+player.toLowerCase()+ChatColor.GOLD+" was unbanned by "+ChatColor.YELLOW+sender.getName());

						}else{
							sender.sendMessage(ChatColor.RED + "Error: This player is permbanned!");
						}

					}else{
						sender.sendMessage(ChatColor.RED + "Error: This player is not banned!");
					}

					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /unban <player>");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
	
	
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}

}
