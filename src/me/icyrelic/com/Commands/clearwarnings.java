package me.icyrelic.com.Commands;


import java.util.UUID;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.PUUID;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class clearwarnings implements CommandExecutor {
	
	LegendaryBans plugin;
	public clearwarnings(LegendaryBans instance) {

		plugin = instance;

		}
	
	private String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "LegendaryBans" + ChatColor.WHITE + "] ");
	
	private LBMySQL mysql = new LBMySQL();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("clearwarnings")) {
			
			if(sender.hasPermission("LegendaryBans.clearwarnings")){
				if(args.length == 1){
					
					String player = args[0];
					
					if(!PUUID.UUIDExists(player)){
						sender.sendMessage(ChatColor.RED + "Error: Could not find player UID please check the username and try again!");
						return true;
					}
					
					UUID uid = null;
					try {
						uid = UUID.fromString(PUUID.getUUID(player));
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(mysql.clearWarnings(uid)){
						sender.sendMessage(prefix+player+"'s warnings have been cleared");
					}else{
						sender.sendMessage(prefix + "Error while trying to clear warnings!");
					}
					
					
					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /clearwarnings <player>");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}

	
	
	
}
