package me.icyrelic.com.Commands;

import java.util.List;
import java.util.UUID;

import me.icyrelic.com.LegendaryBans;
import me.icyrelic.com.PUUID;
import me.icyrelic.com.Data.LBMySQL;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class warn implements CommandExecutor {
	
	LegendaryBans plugin;
	public warn(LegendaryBans instance) {

		plugin = instance;

		}
	
	private LBMySQL mysql = new LBMySQL();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("warn")) {
			
			if(sender.hasPermission("LegendaryBans.warn")){
				//warn player reason
				if(args.length >= 2){
					
					String player = args[0];
					String reason = getFinalArg(args, 1);
					String name = "";
						
					@SuppressWarnings("deprecation")
					List<Player> possible = Bukkit.matchPlayer(args[0]);
					if(possible.size() == 1){
						
						name = possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase();
						
					}else{
						
						name = player;
						
					}
					if(!PUUID.UUIDExists(name)){
						sender.sendMessage(ChatColor.RED + "Error: Could not find player UID please check the username and try again!");
						return true;
					}
					
					UUID uid = null;
					try {
						uid = UUID.fromString(PUUID.getUUID(name));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					
					if(possible.size() == 1){
						for(Player p : Bukkit.getOnlinePlayers()){
							if(p.getName().toLowerCase().equals(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase())){
								plugin.getServer().broadcastMessage(ChatColor.RED+possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase()+ChatColor.GRAY+" has recieved a warning from "+ChatColor.RED+sender.getName());
								plugin.getServer().broadcastMessage(ChatColor.GRAY + ">     "+reason);
								mysql.addWarning(uid, name, sender.getName(), reason);
							}
						}
					}else if (possible.isEmpty()){
						
						sender.sendMessage(ChatColor.RED + "Error: Player Not Found");
						
					}else{
						sender.sendMessage(ChatColor.RED + "Error: More than one player found");
					}
						

					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /warn <player> <reason>");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
	
	
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}

}
