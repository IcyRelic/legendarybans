package me.icyrelic.com.Commands;


import java.util.List;

import me.icyrelic.com.LegendaryBans;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class kick implements CommandExecutor {
	
	LegendaryBans plugin;
	public kick(LegendaryBans instance) {

		plugin = instance;

		}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("kick")) {
			
			if(sender.hasPermission("LegendaryBans.kick")){
				//kick player reason
				if(args.length >= 2){
					
					
					String reason = getFinalArg(args,1);
					List<Player> possible = Bukkit.matchPlayer(args[0]);
					
					if(possible.size() == 1){
						plugin.getServer().getPlayer(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase()).kickPlayer(ChatColor.GOLD +"You were kicked by: "+ ChatColor.YELLOW +sender.getName() + ChatColor.GOLD + " Reason: "+ ChatColor.YELLOW +reason);
					
						plugin.getServer().broadcastMessage(ChatColor.YELLOW+possible.toString().replace("[CraftPlayer{name=", "").replace("}]", "").toLowerCase()+ChatColor.GOLD+" was kicked by "+ChatColor.YELLOW+sender.getName()+ChatColor.GOLD+". Reason: "+ChatColor.YELLOW+reason);
					
					}else{
						sender.sendMessage(ChatColor.RED + "Error: Player not found!");
					}

					
					
					
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /kick <player> <reason>");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
	
}
