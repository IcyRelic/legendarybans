package me.icyrelic.com.Data;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class LBMySQL{
	
	static Connection con = null;
	static Statement st = null;
    static ResultSet rs = null;
	
	
	public boolean connect(String host, String port, String db, String user, String password){
		String prefix = ("[LegendaryBans] ");
	    String url = "jdbc:mysql://"+ host + ":" + port + "/" + db;
		 try {
			con = DriverManager.getConnection(url, user, password);
			 st = con.createStatement();
			 try{
				 
				 System.out.println(prefix+"Loading Tables From Database '"+db+"'");
				 DatabaseMetaData md = con.getMetaData();
				 ResultSet rs = md.getTables(null, null, "banlist", null);
				 if (!rs.next()) {
					System.out.println(prefix+"Creating banlist table");
					  Statement st = con.createStatement();
					  String table = 
							  "CREATE TABLE banlist"+
					                   "(id INTEGER NOT NULL AUTO_INCREMENT, " +
					                   " username VARCHAR(255), " + 
					                   " date BIGINT(255), " + 
					                   " admin VARCHAR(255), " + 
					                   " type VARCHAR(255), " + 
					                   " unban BIGINT(255), " + 
					                   " reason VARCHAR(255), " + 
					                   " PRIMARY KEY ( id ))";
					  st.executeUpdate(table);
				 }else{
					System.out.println(prefix+"Loaded banlist table");
				 }
				 rs = md.getTables(null, null, "warnlist", null);
				 if (!rs.next()) {
					System.out.println(prefix+"Creating warnlist table");
					  Statement st = con.createStatement();
					  String table = 
							  "CREATE TABLE warnlist"+
					                   "(id INTEGER NOT NULL AUTO_INCREMENT, " +
					                   " username VARCHAR(255), " + 
					                   " admin VARCHAR(255), " + 
					                   " reason VARCHAR(255), " +
					                   " date BIGINT(255), " + 
					                   " PRIMARY KEY ( id ))";
					  st.executeUpdate(table);
				 }else{
					System.out.println(prefix+"Loaded warnlist table");
				 }
				 
				 
				  updateDatabase();
				  return true;
			 }
			 catch(SQLException s){
				 return true;
			 }
		} catch (SQLException e) {
			return false;
		}
	}
	
	public static void updateDatabase (){
		boolean didupdates = false;
		int x = 0;
		System.out.println("[LegendaryBans] Checking for database updates");
		
		try{
			DatabaseMetaData md = con.getMetaData();
			ResultSet rs;
			
			rs = md.getColumns(null, null, "banlist", "UUID");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE banlist ADD `UUID` VARCHAR( 255 ) NOT NULL FIRST");
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		if(didupdates == true){
			System.out.println("[LegendaryBans] "+x+" Updates Have Been Added To The Database");
		}else{
			System.out.println("[LegendaryBans] No database updates available");
		}
		 
		
	}
	
	public boolean addWarning(UUID uid, String name, String admin, String reason){
		

        try {
        	String sql = "INSERT INTO warnlist (`UUID` , `id` , `username` , `admin`, `reason`, 	`date`) VALUES ('"+uid+"',NULL,'"+name+"','"+admin+"','"+reason+"','"+System.currentTimeMillis() / 1000+"');";
        	st.executeUpdate(sql);
            return true;

        } catch (SQLException ex) {
            return false;

        }
		
	}
	
	public int countWarningsByAdmin(UUID uid, String admin){
		

        try {
           
            rs = st.executeQuery("SELECT * FROM warnlist WHERE username = '"+uid+"' AND admin = '"+admin+"'");
            
            int count = 0;
            while (rs.next()) {
              count++;
            }  
            
            return count;


        } catch (SQLException ex) {
            return 0;

        }
		
	}
	
	public int countWarnings(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT * FROM warnlist WHERE UUID = '"+uid+"'");
            
            int count = 0;
            while (rs.next()) {
              count++;
            }  
            
            return count;


        } catch (SQLException ex) {
            return 0;

        }
		
	}
	
	public boolean clearWarnings(UUID uid){
		

        try {
        	
        	 String sql = "DELETE FROM warnlist WHERE username = '"+uid+"';";
        	 st.executeUpdate(sql);
        	
        	
        	return true;


        } catch (SQLException ex) {
            return false;

        }
		
	}
	

	public boolean checkBan(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT * FROM banlist WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return true;
            }else{
            	return false;
            }


        } catch (SQLException ex) {
            return false;

        }
		
	}
	
	public int getBanType(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT type FROM banlist WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return Integer.parseInt(rs.getString(1));
            }else{
            	return 0;
            }


        } catch (SQLException ex) {
            return 0;

        }
		
	}
	
	
	public void setBanType(String type, UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT type FROM banlist WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	
            	st.executeUpdate("UPDATE banlist SET type = '"+type+"' WHERE UUID = '"+uid+"'");
            	
            }


        } catch (SQLException ex) {

        }
		
	}
	
	
	public long getDate(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT date FROM banlist WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	
            	if(rs.getString(1).isEmpty()){
            		return 0;
            	}else{
            		return Long.parseLong(rs.getString(1)) * 1000;
            	}
            	
            	
            }else{
            	return 0;
            }


        } catch (SQLException ex) {
            return 0;

        }
		
	}
    
	
	
	public long getUnban(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT unban FROM banlist WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	
            	if(rs.getString(1).isEmpty()){
            		return 0;
            	}else{
            		return Long.parseLong(rs.getString(1)) * 1000;
            	}
            	
            	
            }else{
            	return 0;
            }


        } catch (SQLException ex) {
            return 0;

        }
		
	}
    
	public boolean createBan(UUID uid, String name, String admin, int type, long unban, String reason){
		
		/**
		 * Types
		 * 
		 * Ban - 0
		 * Tempban - 1
		 * Permban - 2
		 */
		
		try {
			String sql = "INSERT INTO banlist (`UUID` , `id` , `username` , `date`, `admin` , `type` , `unban`, `reason`) VALUES ('"+uid+"',NULL,'"+name+"','"+System.currentTimeMillis() / 1000+"','"+admin+"','"+type+"','"+unban/1000+"', '"+reason+"');";

			
			
			st.executeUpdate(sql);
			
			
			return true;
		} catch (SQLException e) {
			e.getStackTrace();
			
			return false;
		}
		

	}
	
	public boolean removeBan(UUID uid){
		
		String sql = "DELETE FROM banlist WHERE UUID = '"+uid+"'";

		try {
			st.executeUpdate(sql);
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		

	}
	
	public String getBanReason(UUID uid){
		
        try {
            
            rs = st.executeQuery("SELECT reason FROM banlist WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	
            		return rs.getString(1);
            	
            }else{
            	return "Unknown";
            }


        } catch (SQLException ex) {
        	return "Unknown";

        }
	}
	
	public String getAdmin(UUID uid){
		
        try {
            
            rs = st.executeQuery("SELECT admin FROM banlist WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	
            		return rs.getString(1);
            	
            }else{
            	return "Unknown";
            }


        } catch (SQLException ex) {
        	return "Unknown";

        }
	}
	
	public void shutdown(String db){
		try {
			
			System.out.println("[LegendaryBans] Disconnected from database '"+db+"'");
			con.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
